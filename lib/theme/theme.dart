import 'package:flutter/material.dart';

class ThemeColor {
  static const Color itemSelected = Color(0xff08EB8C);
  static const Color itemUnSelected = Color(0xffB8BCBC);
  static const Color primaryChart = Color(0xff272323);
  static Color appBackGround = const Color(0xffF6F6F6).withOpacity(0.94);
}
