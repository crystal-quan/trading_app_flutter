// To parse this JSON data, do
//
//     final coinInfo = coinInfoFromJson(jsonString);

import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'coin_info.freezed.dart';
part 'coin_info.g.dart';

List<CoinInfo> coinInfoFromJson(String str) => List<CoinInfo>.from(json.decode(str).map((x) => CoinInfo.fromJson(x)));

String coinInfoToJson(List<CoinInfo> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

@freezed
 class CoinInfo with _$CoinInfo {
    const factory CoinInfo({
        String? id,
        String? name,
        int? yearEstablished,
        String? country,
        String? description,
        String? url,
        String? image,
        bool? hasTradingIncentive,
        int? trustScore,
        int? trustScoreRank,
        double? tradeVolume24HBtc,
        double? tradeVolume24HBtcNormalized,
    }) = _CoinInfo;

    factory CoinInfo.fromJson(Map<String, dynamic> json) => _$CoinInfoFromJson(json);
}
