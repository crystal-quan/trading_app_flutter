// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'coin_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CoinInfo _$CoinInfoFromJson(Map<String, dynamic> json) {
  return _CoinInfo.fromJson(json);
}

/// @nodoc
mixin _$CoinInfo {
  String? get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  int? get yearEstablished => throw _privateConstructorUsedError;
  String? get country => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  String? get url => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;
  bool? get hasTradingIncentive => throw _privateConstructorUsedError;
  int? get trustScore => throw _privateConstructorUsedError;
  int? get trustScoreRank => throw _privateConstructorUsedError;
  double? get tradeVolume24HBtc => throw _privateConstructorUsedError;
  double? get tradeVolume24HBtcNormalized => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CoinInfoCopyWith<CoinInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CoinInfoCopyWith<$Res> {
  factory $CoinInfoCopyWith(CoinInfo value, $Res Function(CoinInfo) then) =
      _$CoinInfoCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      String? name,
      int? yearEstablished,
      String? country,
      String? description,
      String? url,
      String? image,
      bool? hasTradingIncentive,
      int? trustScore,
      int? trustScoreRank,
      double? tradeVolume24HBtc,
      double? tradeVolume24HBtcNormalized});
}

/// @nodoc
class _$CoinInfoCopyWithImpl<$Res> implements $CoinInfoCopyWith<$Res> {
  _$CoinInfoCopyWithImpl(this._value, this._then);

  final CoinInfo _value;
  // ignore: unused_field
  final $Res Function(CoinInfo) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? yearEstablished = freezed,
    Object? country = freezed,
    Object? description = freezed,
    Object? url = freezed,
    Object? image = freezed,
    Object? hasTradingIncentive = freezed,
    Object? trustScore = freezed,
    Object? trustScoreRank = freezed,
    Object? tradeVolume24HBtc = freezed,
    Object? tradeVolume24HBtcNormalized = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      yearEstablished: yearEstablished == freezed
          ? _value.yearEstablished
          : yearEstablished // ignore: cast_nullable_to_non_nullable
              as int?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      hasTradingIncentive: hasTradingIncentive == freezed
          ? _value.hasTradingIncentive
          : hasTradingIncentive // ignore: cast_nullable_to_non_nullable
              as bool?,
      trustScore: trustScore == freezed
          ? _value.trustScore
          : trustScore // ignore: cast_nullable_to_non_nullable
              as int?,
      trustScoreRank: trustScoreRank == freezed
          ? _value.trustScoreRank
          : trustScoreRank // ignore: cast_nullable_to_non_nullable
              as int?,
      tradeVolume24HBtc: tradeVolume24HBtc == freezed
          ? _value.tradeVolume24HBtc
          : tradeVolume24HBtc // ignore: cast_nullable_to_non_nullable
              as double?,
      tradeVolume24HBtcNormalized: tradeVolume24HBtcNormalized == freezed
          ? _value.tradeVolume24HBtcNormalized
          : tradeVolume24HBtcNormalized // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
abstract class _$$_CoinInfoCopyWith<$Res> implements $CoinInfoCopyWith<$Res> {
  factory _$$_CoinInfoCopyWith(
          _$_CoinInfo value, $Res Function(_$_CoinInfo) then) =
      __$$_CoinInfoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? id,
      String? name,
      int? yearEstablished,
      String? country,
      String? description,
      String? url,
      String? image,
      bool? hasTradingIncentive,
      int? trustScore,
      int? trustScoreRank,
      double? tradeVolume24HBtc,
      double? tradeVolume24HBtcNormalized});
}

/// @nodoc
class __$$_CoinInfoCopyWithImpl<$Res> extends _$CoinInfoCopyWithImpl<$Res>
    implements _$$_CoinInfoCopyWith<$Res> {
  __$$_CoinInfoCopyWithImpl(
      _$_CoinInfo _value, $Res Function(_$_CoinInfo) _then)
      : super(_value, (v) => _then(v as _$_CoinInfo));

  @override
  _$_CoinInfo get _value => super._value as _$_CoinInfo;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? yearEstablished = freezed,
    Object? country = freezed,
    Object? description = freezed,
    Object? url = freezed,
    Object? image = freezed,
    Object? hasTradingIncentive = freezed,
    Object? trustScore = freezed,
    Object? trustScoreRank = freezed,
    Object? tradeVolume24HBtc = freezed,
    Object? tradeVolume24HBtcNormalized = freezed,
  }) {
    return _then(_$_CoinInfo(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      yearEstablished: yearEstablished == freezed
          ? _value.yearEstablished
          : yearEstablished // ignore: cast_nullable_to_non_nullable
              as int?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      hasTradingIncentive: hasTradingIncentive == freezed
          ? _value.hasTradingIncentive
          : hasTradingIncentive // ignore: cast_nullable_to_non_nullable
              as bool?,
      trustScore: trustScore == freezed
          ? _value.trustScore
          : trustScore // ignore: cast_nullable_to_non_nullable
              as int?,
      trustScoreRank: trustScoreRank == freezed
          ? _value.trustScoreRank
          : trustScoreRank // ignore: cast_nullable_to_non_nullable
              as int?,
      tradeVolume24HBtc: tradeVolume24HBtc == freezed
          ? _value.tradeVolume24HBtc
          : tradeVolume24HBtc // ignore: cast_nullable_to_non_nullable
              as double?,
      tradeVolume24HBtcNormalized: tradeVolume24HBtcNormalized == freezed
          ? _value.tradeVolume24HBtcNormalized
          : tradeVolume24HBtcNormalized // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CoinInfo implements _CoinInfo {
  const _$_CoinInfo(
      {this.id,
      this.name,
      this.yearEstablished,
      this.country,
      this.description,
      this.url,
      this.image,
      this.hasTradingIncentive,
      this.trustScore,
      this.trustScoreRank,
      this.tradeVolume24HBtc,
      this.tradeVolume24HBtcNormalized});

  factory _$_CoinInfo.fromJson(Map<String, dynamic> json) =>
      _$$_CoinInfoFromJson(json);

  @override
  final String? id;
  @override
  final String? name;
  @override
  final int? yearEstablished;
  @override
  final String? country;
  @override
  final String? description;
  @override
  final String? url;
  @override
  final String? image;
  @override
  final bool? hasTradingIncentive;
  @override
  final int? trustScore;
  @override
  final int? trustScoreRank;
  @override
  final double? tradeVolume24HBtc;
  @override
  final double? tradeVolume24HBtcNormalized;

  @override
  String toString() {
    return 'CoinInfo(id: $id, name: $name, yearEstablished: $yearEstablished, country: $country, description: $description, url: $url, image: $image, hasTradingIncentive: $hasTradingIncentive, trustScore: $trustScore, trustScoreRank: $trustScoreRank, tradeVolume24HBtc: $tradeVolume24HBtc, tradeVolume24HBtcNormalized: $tradeVolume24HBtcNormalized)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CoinInfo &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality()
                .equals(other.yearEstablished, yearEstablished) &&
            const DeepCollectionEquality().equals(other.country, country) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.url, url) &&
            const DeepCollectionEquality().equals(other.image, image) &&
            const DeepCollectionEquality()
                .equals(other.hasTradingIncentive, hasTradingIncentive) &&
            const DeepCollectionEquality()
                .equals(other.trustScore, trustScore) &&
            const DeepCollectionEquality()
                .equals(other.trustScoreRank, trustScoreRank) &&
            const DeepCollectionEquality()
                .equals(other.tradeVolume24HBtc, tradeVolume24HBtc) &&
            const DeepCollectionEquality().equals(
                other.tradeVolume24HBtcNormalized,
                tradeVolume24HBtcNormalized));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(yearEstablished),
      const DeepCollectionEquality().hash(country),
      const DeepCollectionEquality().hash(description),
      const DeepCollectionEquality().hash(url),
      const DeepCollectionEquality().hash(image),
      const DeepCollectionEquality().hash(hasTradingIncentive),
      const DeepCollectionEquality().hash(trustScore),
      const DeepCollectionEquality().hash(trustScoreRank),
      const DeepCollectionEquality().hash(tradeVolume24HBtc),
      const DeepCollectionEquality().hash(tradeVolume24HBtcNormalized));

  @JsonKey(ignore: true)
  @override
  _$$_CoinInfoCopyWith<_$_CoinInfo> get copyWith =>
      __$$_CoinInfoCopyWithImpl<_$_CoinInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CoinInfoToJson(
      this,
    );
  }
}

abstract class _CoinInfo implements CoinInfo {
  const factory _CoinInfo(
      {final String? id,
      final String? name,
      final int? yearEstablished,
      final String? country,
      final String? description,
      final String? url,
      final String? image,
      final bool? hasTradingIncentive,
      final int? trustScore,
      final int? trustScoreRank,
      final double? tradeVolume24HBtc,
      final double? tradeVolume24HBtcNormalized}) = _$_CoinInfo;

  factory _CoinInfo.fromJson(Map<String, dynamic> json) = _$_CoinInfo.fromJson;

  @override
  String? get id;
  @override
  String? get name;
  @override
  int? get yearEstablished;
  @override
  String? get country;
  @override
  String? get description;
  @override
  String? get url;
  @override
  String? get image;
  @override
  bool? get hasTradingIncentive;
  @override
  int? get trustScore;
  @override
  int? get trustScoreRank;
  @override
  double? get tradeVolume24HBtc;
  @override
  double? get tradeVolume24HBtcNormalized;
  @override
  @JsonKey(ignore: true)
  _$$_CoinInfoCopyWith<_$_CoinInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
