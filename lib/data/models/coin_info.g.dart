// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coin_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CoinInfo _$$_CoinInfoFromJson(Map<String, dynamic> json) => _$_CoinInfo(
      id: json['id'] as String?,
      name: json['name'] as String?,
      yearEstablished: json['yearEstablished'] as int?,
      country: json['country'] as String?,
      description: json['description'] as String?,
      url: json['url'] as String?,
      image: json['image'] as String?,
      hasTradingIncentive: json['hasTradingIncentive'] as bool?,
      trustScore: json['trustScore'] as int?,
      trustScoreRank: json['trustScoreRank'] as int?,
      tradeVolume24HBtc: (json['tradeVolume24HBtc'] as num?)?.toDouble(),
      tradeVolume24HBtcNormalized:
          (json['tradeVolume24HBtcNormalized'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$_CoinInfoToJson(_$_CoinInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'yearEstablished': instance.yearEstablished,
      'country': instance.country,
      'description': instance.description,
      'url': instance.url,
      'image': instance.image,
      'hasTradingIncentive': instance.hasTradingIncentive,
      'trustScore': instance.trustScore,
      'trustScoreRank': instance.trustScoreRank,
      'tradeVolume24HBtc': instance.tradeVolume24HBtc,
      'tradeVolume24HBtcNormalized': instance.tradeVolume24HBtcNormalized,
    };
