import 'package:dio/dio.dart' as dio;
import 'package:retrofit/retrofit.dart';
import 'package:trading_app_flutter/data/models/coin_info.dart';



part 'api.g.dart';

const String baseUrl = 'https://api.coingecko.com';
const String apiKey = 'd8425b2f-a5c2-4069-9527-cd444d7d6e07';
const String baseImageUrl = 'https://image.tmdb.org/t/p/w500';
const String headers = 'X-CMC_PRO_API_KEY';
const String endpointsUrl = '/v1/cryptocurrency/listings/latest';

@RestApi(baseUrl: baseUrl)
abstract class ApiClient {
  factory ApiClient(dio.Dio dio, {String baseUrl}) = _ApiClient;

  @Headers(<String, dynamic>{
    "Content-Type": "application/json",

  })
  @GET("/api/v3/exchanges")
  Future<CoinInfo> getCoinIfo();

  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
  })
  
}
