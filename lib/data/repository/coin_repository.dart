import 'package:dio/dio.dart';
import 'package:trading_app_flutter/data/models/coin_info.dart';

import '../api/api.dart';

class CoinRepository {
  final ApiClient apiClient = ApiClient(Dio());
  Future<CoinInfo>? getCoinInfo() async {
    return await apiClient.getCoinIfo();
  }

}
// class CoinRepositoryImpl extends CoinRepository {
//   late ApiClient _apiClient;

//   CoinRepositoryImpl(ApiClient client) {
//     _apiClient = client;
//   }

//   @override
//   Future<CoinIdMap>? getCoinIdMap() async {
  
//   }

//   @override
//   Future<CoinInfomation>? getCoinInfomation(String coinId) async {
//     return await _apiClient.getCoinInfomation(coinId);
//   }

//   @override
//   Future<LatestListings>? getLatestListings() async {
//     return await _apiClient.getLatestListings();
//   }

// }
