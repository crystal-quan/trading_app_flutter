import 'package:trading_app_flutter/data/repository/coin_repository.dart';

class HomeRepository {
  final CoinRepository coinRepository = CoinRepository();
  late List<String> coinID;
  late List<String> coinLogo;
  late List<String> coinName;
  late List<String> coinSymbol;
  late List<double> coinPrice;
}

class HomeData {
  HomeData({this.coinLogo, this.coinName, this.coinSymbol, this.coinPrice});
  List<String>? coinLogo;
  List<String>? coinName;
  List<String>? coinSymbol;
  List<double>? coinPrice;
  List<double>? volumeChange;
  List<double>? percentChange;
}
