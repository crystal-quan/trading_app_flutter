import 'package:flutter/material.dart';
import 'package:trading_app_flutter/UI/pie_chart/widget_line_chart.dart';
import 'dart:math' as math;

import 'package:trading_app_flutter/theme/theme.dart';

class ShowLineChart extends StatefulWidget {
  const ShowLineChart();

  @override
  State<ShowLineChart> createState() => _ShowLineChartState();
}

class _ShowLineChartState extends State<ShowLineChart> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Material(
      color: ThemeColor.appBackGround,
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: screenWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 13, vertical: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: const Icon(Icons.arrow_back_ios_new),
                          padding: EdgeInsets.zero,
                          constraints:
                              const BoxConstraints(minHeight: 0, minWidth: 0),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 16, right: 7),
                          child: Transform.rotate(
                              angle: math.pi / 5.0,
                              child: Image.asset(
                                'assets/images/btc.png',
                                height: 30,
                              )),
                        ),
                        const Text(
                          'Bitcoin',
                          style: TextStyle(
                            color: Color(0xff212529),
                            fontSize: 16,
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 4, right: 10),
                          child: Text(
                            '(BTC)',
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                color: Color(0xff6C757D),
                                fontSize: 10,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        const Icon(Icons.star_border),
                      ],
                    ),
                    Container(
                      alignment: Alignment.centerRight,
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        decoration: const BoxDecoration(
                            color: Color(0xffECF4FF),
                            borderRadius:
                                BorderRadius.all(Radius.circular(48))),
                        child: Row(
                          children: [
                            Image.asset('assets/images/ic_vector.png'),
                            const Text(
                              ' Exchange',
                              style: TextStyle(color: Color(0xff0063F5)),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              RichText(
                text: const TextSpan(
                    text: '₹98,509.75      ',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.w500),
                    children: [
                      TextSpan(
                          text: '+ 1700.254 (9.77%)',
                          style: TextStyle(
                              color: ThemeColor.itemSelected,
                              fontSize: 16,
                              fontWeight: FontWeight.w500))
                    ]),
              ),
              SizedBox(
                width: screenWidth,
                height: screenHeight / 2.9,
                child: LineChartWidget.withSampleData(infomation: true),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const [
                    CustomTextButton(
                      centerTitle: '1 H',
                    ),
                    CustomTextButton(
                      centerTitle: '24 H',
                    ),
                    CustomTextButton(
                      centerTitle: '1 W',
                    ),
                    CustomTextButton(
                      centerTitle: '1 M',
                    ),
                    CustomTextButton(
                      centerTitle: '6 M',
                    ),
                    CustomTextButton(
                      centerTitle: '1 Y',
                    ),
                    CustomTextButton(
                      centerTitle: 'All',
                    ),
                  ],
                ),
              ),
              Container(
                // width: double.infinity,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Colors.white),
                margin: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 13, vertical: 6),
                      child: Transform.rotate(
                          angle: math.pi / 5.0,
                          child: Image.asset(
                            'assets/images/2x/btc.png',
                            height: 40,
                          )),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          RichText(
                            text: const TextSpan(
                                text: 'Bitcoin\n',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500),
                                children: [
                                  TextSpan(
                                      text: '00.00 BTC',
                                      style: TextStyle(
                                          color: Color(0xff6C757D),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500))
                                ]),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: RichText(
                              textAlign: TextAlign.end,
                              text: const TextSpan(
                                  text: '₹00.00\n',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500),
                                  children: [
                                    TextSpan(
                                      text: '00.00%',
                                      style: TextStyle(
                                          color: Color(0xff6C757D),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ]),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(2),
                  ),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Transactions'),
                    InkWell(
                      onTap: () {},
                      child: const Icon(Icons.arrow_forward_ios),
                    )
                  ],
                ),
              ),
              Container(
                width: screenWidth,
                padding: const EdgeInsets.all(20),
                child: const Text(
                  'Market Stats',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 20),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      customRow(
                          'ic_market.png', 'Market Cap', '₹19.8 trillion'),
                      const SizedBox(
                        height: 10,
                      ),
                      customRow('ic_volume.png', 'Volume', '₹4.1 trillion'),
                      const SizedBox(
                        height: 10,
                      ),
                      customRow('ic_circulating.png', 'Circulating Supply',
                          '116.0 million'),
                      const SizedBox(
                        height: 10,
                      ),
                      customRow('ic_popularity.png', 'Popularity', '#2'),
                      const SizedBox(
                        height: 10,
                      ),
                      const Divider(
                        color: Color(0xffDFE2E4),
                      ),
                      SizedBox(
                        height: 30,
                        width: screenWidth,
                        child: const Text(
                          'About',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(
                        width: screenWidth,
                        child: const Text(
                          'The world’s first cryptocurrency, Bitcoin is stored and exchanged securely on the internet through a digital ledger known as a blockchain. Bitcoins are divisible into smaller units known as satoshis — each satoshi is worth 0.00000001 bitcoin. Read more',
                          style: TextStyle(
                              color: Color(0xff6C757D),
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                      )
                    ]),
              )
            ],
          ),
        ),
      ),
    );
  }

  Row customRow(String icon, String startTitle, String endTitle) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Image.asset('assets/images/$icon'),
            const SizedBox(
              width: 7,
            ),
            Text(
              '$startTitle',
              style: const TextStyle(
                  color: Color(0xff6C757D),
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            ),
          ],
        ),
        Text(
          '$endTitle',
          style: const TextStyle(
              color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500),
        ),
      ],
    );
  }
}

////
class CustomTextButton extends StatelessWidget {
  final String centerTitle;
  final dynamic ontap;
  const CustomTextButton({
    this.ontap,
    required this.centerTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ontap,
      child: Container(
        alignment: Alignment.center,
        width: 42,
        height: 27,
        decoration: BoxDecoration(
          color: const Color(0xffECF4FF),
          border: Border.all(
            color: const Color(0xff0063F5),
          ),
          borderRadius: const BorderRadius.all(Radius.circular(20)),
        ),
        child: Text(
          centerTitle,
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: Color(0xff0063F5),
          ),
        ),
      ),
    );
  }
}
