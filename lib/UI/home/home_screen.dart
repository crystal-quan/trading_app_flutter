import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trading_app_flutter/UI/home/bloc/home_bloc.dart';
import 'package:trading_app_flutter/theme/theme.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(),
      child: const HomeScreen(),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with AutomaticKeepAliveClientMixin {
  final HomeBloc homeBloc = HomeBloc();
  @override
  void initState() {
    super.initState();
    homeBloc.add(GetCoinIfomation());
  }

  @override
  Widget build(BuildContext context) {
    final screenWith = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: ThemeColor.appBackGround,
      body: SafeArea(
        top: false,
        child: CustomScrollView(
          primary: true,
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                width: screenWith,
                height: 30,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      const Color(0xff08EB8C).withOpacity(1),
                      const Color(0xff0DF076).withOpacity(0.27),
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: const [0.1, 1.0],
                  ),
                ),
              ),
            ),
            SliverAppBar(
              backgroundColor: ThemeColor.itemSelected,
              primary: true,
              pinned: true,
              snap: true,
              floating: true,
              expandedHeight: 100,
              toolbarHeight: 80,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 36),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'CoinPro',
                        style: TextStyle(
                          fontSize: 35,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(right: 20),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          color: const Color(0xff00FE96),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.8),
                              blurStyle: BlurStyle.normal,
                              blurRadius: 5,
                              spreadRadius: 1,
                              offset: const Offset(3, 4),
                            ),
                          ],
                          borderRadius: const BorderRadius.all(
                            Radius.circular(50),
                          ),
                        ),
                        child: Image.asset('assets/images/ic_notification.png'),
                      ),
                    ],
                  ),
                ),
                expandedTitleScale: 1,
                background: Container(
                  color: Colors.white,
                  child: Container(
                    width: screenWith,
                    height: 100,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          const Color(0xff08EB8C).withOpacity(1),
                          const Color(0xff0DF076).withOpacity(0.25),
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        stops: const [0.1, 1.0],
                      ),
                    ),
                    child: const Padding(
                      padding: EdgeInsets.only(left: 36, top: 8),
                      child: Text(
                        'Welcome to',
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w300,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.topCenter,
                        height: 110,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              const Color(0xff08EB8C).withOpacity(1),
                              const Color(0xff0DF076).withOpacity(0.15),
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            stops: const [0.1, 1.0],
                          ),
                        ),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 36, vertical: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                              text: const TextSpan(
                                  text: 'My Balance\n',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w300,
                                      height: 1.5),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: '\$ 69,32.69',
                                      style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ]),
                            ),
                            Container(
                              width: 80,
                              height: 30,
                              decoration: BoxDecoration(
                                color: const Color(0xff00FE96),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.8),
                                    blurStyle: BlurStyle.normal,
                                    blurRadius: 5,
                                    spreadRadius: 1,
                                    offset: const Offset(3, 4),
                                  ),
                                ],
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(50),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/ic_eject.png'),
                                  const Text(
                                    '20%',
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 110,
                        padding: const EdgeInsets.only(
                            top: 20, left: 39, right: 39, bottom: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              'My portfolio',
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black),
                            ),
                            TextButton(
                              onPressed: () {},
                              child: const Text(
                                'See all',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    color: ThemeColor.itemSelected),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 70,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.8),
                            blurStyle: BlurStyle.normal,
                            blurRadius: 4,
                            spreadRadius: 2,
                            offset: const Offset(0, 7)),
                      ],
                      borderRadius: const BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    margin: const EdgeInsets.only(
                      left: 35,
                      right: 35,
                    ),
                    child: Row(
                      children: [
                        Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                                margin: const EdgeInsets.only(
                                    top: 11, bottom: 11, right: 7, left: 16),
                                decoration: BoxDecoration(
                                  color:
                                      ThemeColor.itemSelected ?? Colors.white,
                                  border: Border.all(
                                    width: 1.0,
                                    strokeAlign: StrokeAlign.outside,
                                    color: ThemeColor.itemSelected,
                                  ),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Stack(
                                  fit: StackFit.expand,
                                  children: const [
                                    Center(
                                      child: Text(
                                        'Deposit INR',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            color:
                                                // ThemeColor().itemSelected ??
                                                Colors.white),
                                      ),
                                    ),
                                  ],
                                )),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                                margin: const EdgeInsets.only(
                                    top: 11, bottom: 11, right: 16, left: 7),
                                decoration: BoxDecoration(
                                  color:
                                      ThemeColor.itemSelected ?? Colors.white,
                                  border: Border.all(
                                    width: 1.0,
                                    strokeAlign: StrokeAlign.outside,
                                    color: ThemeColor.itemSelected,
                                  ),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Stack(
                                  fit: StackFit.loose,
                                  children: const [
                                    Center(
                                      child: Text(
                                        'Withdraw INR',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            color:
                                                // ThemeColor().itemSelected ??
                                                Colors.white),
                                      ),
                                    ),
                                  ],
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SliverList(
                delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 26, vertical: 6),
                  width: screenWith,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.8),
                          blurStyle: BlurStyle.normal,
                          blurRadius: 4,
                          spreadRadius: 2,
                          offset: const Offset(3, 4)),
                    ],
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: BlocBuilder<HomeBloc, HomeState>(
                      bloc: homeBloc,
                      buildWhen: (previous, current) =>
                          previous.loadStatus != current.loadStatus,
                      builder: (context, state) {
                        return Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(12),
                              decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  color: Color(0xffF7F7F7)),
                              child: Image.network(
                                state
                                        .coinShortInfo
                                        ?.data?['${state.coinId![index]}']
                                        ?.logo ??
                                    'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Error.svg/2198px-Error.svg.png',
                                width: 50,
                                height: 50,
                              ),
                            ),
                            Expanded(
                              child: Container(
                                margin:
                                    const EdgeInsets.only(left: 13, right: 0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          '${state.coinShortInfo?.data?['${state.coinId![index]}']?.symbol}'
                                          '/BUSD',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: 20,
                                          ),
                                        ),
                                        Text(
                                          '\,382.64',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          '${state.coinShortInfo?.data?['${state.coinId![index]}']?.name}',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            color: Color(0xff5D5C5D),
                                          ),
                                        ),
                                        Container(
                                          width: 55,
                                          height: 22,
                                          decoration: const BoxDecoration(
                                            color: ThemeColor.itemSelected,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(50)),
                                          ),
                                          child: Row(
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(4.0),
                                                child: Image.asset(
                                                    'assets/images/ic_arrow_up.png'),
                                              ),
                                              const Text(
                                                '15.3%',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10,
                                                    fontWeight:
                                                        FontWeight.w700),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        );
                      },
                    ),
                  ),
                );
              },
              childCount: 50,
              addAutomaticKeepAlives: true,
            ))
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
