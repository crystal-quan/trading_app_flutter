part of 'home_bloc.dart';

class HomeState extends Equatable {
  const HomeState(
      {this.coinId, this.coinShortInfo, this.loadStatus = LoadStatus.INITIAL});
  final List<String>? coinId;
  final CoinShortInfo? coinShortInfo;
  

  final LoadStatus loadStatus;
  HomeState copyWith({
    List<String>? coinId,
    CoinShortInfo? coinShortInfo,
    LoadStatus? loadStatus,
  }) {
    return HomeState(
        coinId: coinId ?? this.coinId,
        coinShortInfo: coinShortInfo ?? this.coinShortInfo,
        loadStatus: loadStatus ?? this.loadStatus);
  }

  @override
  // TODO: implement props
  List<Object?> get props => [loadStatus, coinId, coinShortInfo];
}
