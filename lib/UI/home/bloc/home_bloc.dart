import 'dart:developer';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trading_app_flutter/data/models/coin_id_map/coin_id_map.dart'
    as mapID;
import 'package:trading_app_flutter/data/models/coin_quotes.dart/coin_quotes.dart';
import 'package:trading_app_flutter/data/models/coin_short_info/coin_short_info.dart';
import 'package:trading_app_flutter/data/models/load_status.dart';
import 'package:trading_app_flutter/data/repository/coin_repository.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(const HomeState()) {
    on<GetCoinIfomation>(_onGetCoinInfomation);
  }

  final CoinRepository coinRepository = CoinRepository();

  void _onGetCoinInfomation(
    GetCoinIfomation event,
    Emitter<HomeState> emit,
  ) async {
    // emit(state.copyWith(loadStatus: LoadStatus.LOADING));
    try {
      List<mapID.Datum> softRank = [];
      final idMap = await coinRepository.getCoinIdMap();
      idMap?.data?.forEach((e) {
        softRank.add(e);
      });
      softRank.sort((a, b) => a.rank! - b.rank!);
      final coinID = softRank.map((e) {
        return e.id.toString();
      }).toList();
      print('quanquan check rank $softRank');

      String url = coinID[0];
      print('quan bv check state list ID - ${coinID}');
      for (var i = 1; i < 50; i++) {
        url = url + ',' + coinID[i].toString();
      }
      print(url);
      final listInfo = await coinRepository.getCoinShotInfo(coinId: url);
      final coinQuotes = await coinRepository.getCoinQuotes(coinId: url);
      


      emit(state.copyWith(
          coinId: coinID,
          coinShortInfo: listInfo,
          loadStatus: LoadStatus.SUCCESS));

      print('quan check state${state.coinShortInfo}');
    } catch (e, s) {
      log(e.toString(), error: e, stackTrace: s);
      emit(state.copyWith(loadStatus: LoadStatus.FAILURE));
    }
    print(state.loadStatus);
  }
}
