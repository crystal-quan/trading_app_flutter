import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart' as pie;
import 'package:trading_app_flutter/UI/pie_chart/widget_line_chart.dart';
import 'package:trading_app_flutter/UI/show_line_chart/show_line_chart.dart';
import 'package:trading_app_flutter/theme/theme.dart';

class PieChartScreen extends StatefulWidget {
  const PieChartScreen({Key? key}) : super(key: key);

  @override
  State<PieChartScreen> createState() => _PieChartScreenState();
}

class _PieChartScreenState extends State<PieChartScreen> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              width: screenWidth,
              height: screenHeight / 4,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [
                        Expanded(
                          child: Container(
                            width: double.infinity,
                            height: double.infinity,
                            margin: const EdgeInsets.only(left: 9, top: 10),
                            decoration: const BoxDecoration(
                              color: ThemeColor.itemSelected,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(48)),
                            ),
                            child: FittedBox(
                              fit: BoxFit.contain,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                            'assets/images/ic_transaction.png'),
                                        const Text(
                                          '17',
                                          style: TextStyle(
                                              fontSize: 24,
                                              fontWeight: FontWeight.w700,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                    const Text(
                                      'Total transactions\nThis week',
                                      style: TextStyle(fontSize: 14),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            width: double.infinity,
                            height: double.infinity,
                            margin: const EdgeInsets.only(left: 9, top: 10),
                            decoration: const BoxDecoration(
                              color: Color(0xff17223B),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(48)),
                            ),
                            child: FittedBox(
                              child: Row(
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Send',
                                      style:
                                          TextStyle(color: Color(0xff9FA2B2)),
                                    ),
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.all(10),
                                      child: Image.asset(
                                        'assets/images/ic_send.png',
                                        width: 20,
                                        height: 20,
                                      ))
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Stack(
                      children: [
                        pie.PieChart(
                          dataMap: const <String, double>{'a': 70, 'empty': 30},
                          animationDuration: const Duration(milliseconds: 800),
                          chartLegendSpacing: 13,
                          emptyColor: Colors.black,
                          chartRadius: MediaQuery.of(context).size.width / 2.6,
                          initialAngleInDegree: 270,
                          chartType: pie.ChartType.ring,
                          ringStrokeWidth: 12,
                          colorList: const [
                            ThemeColor.itemSelected,
                            Color(0xff17223B)
                          ],
                          centerText: const TextSpan(
                            text: '',
                            style: TextStyle(
                                fontSize: 6,
                                fontWeight: FontWeight.w700,
                                color: Color(0xffBCB8B8)),
                          ).text,
                          centerTextStyle: const TextStyle(
                              fontSize: 36,
                              fontWeight: FontWeight.w700,
                              color: Color(0xffBCB8B8)),
                          degreeOptions:
                              const pie.DegreeOptions(initialAngle: 0),
                          legendOptions: const pie.LegendOptions(
                            showLegendsInRow: false,
                            legendPosition: pie.LegendPosition.right,
                            showLegends: false,
                            legendShape: BoxShape.circle,
                            legendTextStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          chartValuesOptions: const pie.ChartValuesOptions(
                            showChartValueBackground: false,
                            showChartValues: false,
                            showChartValuesInPercentage: true,
                            showChartValuesOutside: false,
                            decimalPlaces: 1,
                          ),
                        ),
                        Center(
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text: '70%\n',
                                style: TextStyle(
                                    color: const Color(0xffBCB8B8),
                                    fontSize: screenHeight / 24,
                                    fontWeight: FontWeight.w700),
                                children: [
                                  TextSpan(
                                    text: "Limit",
                                    style: TextStyle(
                                        color: const Color(0xffBCB8B8),
                                        fontSize: screenHeight / 35.2,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15, left: 21, right: 15),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Charts',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff272323)),
                      ),
                      TextButton(
                        onPressed: () {},
                        child: const Text(
                          'See all',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff272323)),
                        ),
                      ),
                    ],
                  ),
                  ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return FittedBox(
                        child: InkWell(
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const ShowLineChart(),
                          )),
                          child: Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 25),
                                decoration: BoxDecoration(
                                    color: const Color(0xff272323)
                                        .withOpacity(0.1),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(20))),
                                child: Image.asset(
                                  'assets/images/ic_logo_bitcoin.png',
                                  // fit: BoxFit.fitWidth,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 13),
                                child: RichText(
                                  text: const TextSpan(
                                      text: 'BTC\n',
                                      style: TextStyle(
                                        height: 1.5,
                                        fontSize: 16,
                                        color: ThemeColor.primaryChart,
                                      ),
                                      children: [
                                        TextSpan(
                                            text: '+1,6%',
                                            style: TextStyle(
                                                height: 2.5,
                                                fontSize: 12,
                                                color: ThemeColor.primaryChart))
                                      ]),
                                ),
                              ),
                              SizedBox(
                                width: screenWidth / 2.7,
                                height: 90,
                                child: LineChartWidget.withSampleData(),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 6),
                                child: RichText(
                                  text: const TextSpan(
                                      text: '\$29,850.15 \n',
                                      style: TextStyle(
                                        height: 1.5,
                                        fontSize: 16,
                                        color: ThemeColor.primaryChart,
                                      ),
                                      children: [
                                        TextSpan(
                                            text: '2.73 BTC',
                                            style: TextStyle(
                                                height: 2.5,
                                                fontSize: 12,
                                                color: ThemeColor.primaryChart))
                                      ]),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) =>
                        const Divider(color: Color(0xff272323)),
                    itemCount: 10,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
