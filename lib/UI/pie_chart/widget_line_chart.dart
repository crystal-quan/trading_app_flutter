/// Example of timeseries chart with a custom number of ticks
///
/// The tick count can be set by setting the [desiredMinTickCount] and
/// [desiredMaxTickCount] for automatically adjusted tick counts (based on
/// how 'nice' the ticks are) or [desiredTickCount] for a fixed tick count.
// ignore_for_file: must_be_immutable

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class LineChartWidget extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  List<MyRow>? data;
  bool infomation;

  LineChartWidget(this.seriesList,
      {required this.animate, this.data, required this.infomation});

  /// Creates a [TimeSeriesChart] with sample data and no transition.
  factory LineChartWidget.withSampleData(
      {List<MyRow>? data, bool? infomation}) {
    return LineChartWidget(
      _createSampleData(data: data),
      infomation: infomation ?? false,
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return charts.TimeSeriesChart(_createSampleData(),
        behaviors: (infomation)
            ? [
                charts.RangeAnnotation([
                  charts.LineAnnotationSegment(DateTime(2017, 10, 02),
                      charts.RangeAnnotationAxisType.domain,
                      labelDirection:
                          charts.AnnotationLabelDirection.horizontal,
                      endLabel: 'Oct 4'),
                  charts.LineAnnotationSegment(DateTime(2017, 9, 28),
                      charts.RangeAnnotationAxisType.domain,
                      labelDirection:
                          charts.AnnotationLabelDirection.horizontal,
                      startLabel: 'Oct 15'),
                ]),
              ]
            : [],
        animate: animate,

        /// Customize the measure axis to have 2 ticks,
        primaryMeasureAxis: const charts.NumericAxisSpec(
            tickProviderSpec:
                charts.BasicNumericTickProviderSpec(desiredTickCount: 2)));
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<MyRow, DateTime>> _createSampleData(
      {List<MyRow>? data}) {
    final myRowData = data ??
        [
          MyRow(DateTime(2017, 9, 25), 10),
          MyRow(DateTime(2017, 9, 26), 18),
          MyRow(DateTime(2017, 9, 27), 16),
          MyRow(DateTime(2017, 9, 28), 9),
          MyRow(DateTime(2017, 9, 29), 11),
          MyRow(DateTime(2017, 9, 30), 15),
          MyRow(DateTime(2017, 10, 01), 25),
          MyRow(DateTime(2017, 10, 02), 33),
          MyRow(DateTime(2017, 10, 03), 30),
          MyRow(DateTime(2017, 10, 04), 31),
          MyRow(DateTime(2017, 10, 08), 23),
          MyRow(DateTime(2017, 10, 12), 25),
          MyRow(DateTime(2017, 10, 16), 20),
        ];

    return [
      charts.Series<MyRow, DateTime>(
        id: 'Cost',
        domainFn: (MyRow row, _) => row.timeStamp,
        measureFn: (MyRow row, _) => row.cost,
        data: myRowData,
      )
    ];
  }
}

/// Sample time series data type.
class MyRow {
  final DateTime timeStamp;
  final int cost;
  MyRow(this.timeStamp, this.cost);
}
