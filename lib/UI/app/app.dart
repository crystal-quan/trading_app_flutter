import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:trading_app_flutter/UI/home/home_screen.dart';
import 'package:trading_app_flutter/UI/pie_chart/pie_chart_screen.dart';
import 'package:trading_app_flutter/theme/theme.dart';

import 'bloc/app_bloc.dart';

class App extends StatelessWidget {
  const App({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AppBloc(),
      child: const AppView(),
    );
  }
}

class AppView extends StatefulWidget {
  const AppView({
    Key? key,
  }) : super(key: key);

  @override
  State<AppView> createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  @override
  Widget build(BuildContext context) {
    List<Widget> listPage = <Widget>[
      const HomePage(),
      const HomePage(),
      const PieChartScreen(),
      const HomePage(),
    ];
    return MaterialApp(
      useInheritedMediaQuery: true,
      locale: DevicePreview.locale(context),
      builder: DevicePreview.appBuilder,
      theme: ThemeData(
        textTheme: GoogleFonts.robotoTextTheme(Theme.of(context).textTheme),
      ),
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<AppBloc, AppState>(
        buildWhen: (previous, current) =>
            previous.pageIndex != current.pageIndex,
        builder: (context, state) {
          return Scaffold(
            backgroundColor: const Color(0xffE5E5E5),
            // extendBody: true,
            // extendBodyBehindAppBar: true,
            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              iconSize: 5,
              showSelectedLabels: false,
              showUnselectedLabels: false,
              elevation: 0,
              items: [
                BottomNavigationBarItem(
                    icon: Image.asset(
                      'assets/images/ic_home.png',
                      color: (state.pageIndex == 0)
                          ? ThemeColor.itemSelected
                          : ThemeColor.itemUnSelected,
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Image.asset(
                      'assets/images/ic_menu2.png',
                      color: (state.pageIndex == 1)
                          ? ThemeColor.itemSelected
                          : ThemeColor.itemUnSelected,
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Image.asset(
                      'assets/images/ic_menu3.png',
                      color: (state.pageIndex == 2)
                          ? ThemeColor.itemSelected
                          : ThemeColor.itemUnSelected,
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Image.asset(
                      'assets/images/ic_setting.png',
                      color: (state.pageIndex == 3)
                          ? ThemeColor.itemSelected
                          : ThemeColor.itemUnSelected,
                    ),
                    label: ''),
              ],
              currentIndex: 0,
              selectedFontSize: 14,
              unselectedFontSize: 14,
              onTap: (value) =>
                  context.read<AppBloc>().add(SelectPage(selectPage: value)),
            ),
            body: SafeArea(top: false, child: listPage[state.pageIndex]),
          );
        },
      ),
    );
  }
}
