// ignore_for_file: must_be_immutable

part of 'app_bloc.dart';

@immutable
abstract class AppEvent {}

class SelectPage extends AppEvent {
  int? selectPage;
  SelectPage({this.selectPage});
}
