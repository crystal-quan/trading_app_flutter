// ignore_for_file: must_be_immutable

part of 'app_bloc.dart';

class AppState extends Equatable {
  AppState({this.pageIndex = 0});
  int pageIndex;

  AppState copyWith({int? pageIndex}) {
    return AppState(pageIndex: pageIndex ?? this.pageIndex);
  }

  @override
  List<Object?> get props => [pageIndex];
}
