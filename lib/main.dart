import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trading_app_flutter/bloc_observer.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import 'UI/app/app.dart';

void main() {
  Bloc.observer = AppBlocObserver();
  runApp(DevicePreview(
    enabled: kIsWeb,
    builder: (context) => const App(),
  ));
}
